# Star-Camera

Star-Camera is C library whose purpose is to simulate camera model.

## How to build

Star-Camera relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build. It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/) library.

First ensure that CMake is installed on your system. Then install the RCMake
package and the RSys library. Finally generate the project from the
`cmake/CMakeLists.txt` file by appending to the `CMAKE_PREFIX_PATH` variable
the install directories of its dependencies. The resulting project can be
edited, built, tested and installed as any CMake project. Refer to the
[CMake](https://cmake.org/documentation) for further informations on CMake.

## License

Copyright (C) 2021 [|Meso|Star>](https://www.meso-star.com)
(<contact@meso-star.com>). Star-Camera is free software
released under the GPL v3+ license: GNU GPL version 3 or later. You are welcome
to redistribute it under certain conditions; refer to the COPYING file for
details.
